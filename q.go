package q

import (
	"fmt"
)

type Hello interface {
	Greet()
}

type svc struct {
	str string
}

func (s *svc) Greet() {
	fmt.Println(s.str)
}

func New() Hello {
	return &svc{"oh hello there"}
}
